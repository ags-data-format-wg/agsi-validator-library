# agsi-validator-library

## Introduction

The agsi-validator library is an open source project to provide the ability to validate AGSi files.

The AGSi format is documented here: 

https://www.ags.org.uk/data-format/agsi-ground-model/


https://ags-data-format-wg.gitlab.io/AGSi_Documentation/

## status
As at June 2023 the project is in developmentand test stage. It must not be used for production work.

## Installation
The library is simple, with few dependencies. See the Requirements.txt file for additional packages. 

## Usage
Usage will be developed over time. Curently the library can be tested by running Python main.py

## Support
Use the Issues to ask for help.

## Roadmap
The current roadmap is still being drawn up. The the idea is to provide a library that can be used by diferent application to check AGSi files.

## Contributing
All contributions are welcome, but don't expect an instant response. Please Fork, branch and use a merge request.

## Authors and acknowledgment
Many thanks to Neil Chadwick digitalgeotechnical.com for kicking off the codebase.

## License
The licence is LGPL 3


