# -*- coding: utf-8 -*-
"""
Created on Fri Feb  3 21:13:01 2023

@author: DigitalGeotechnical
# This file is part of agsi-validator.
#
# agsi-validator is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# https://gitlab.com/ags-data-format-wg/agsi-validator-library

"""

# Libraries used by these functions
from jsonschema import Draft201909Validator, SchemaError, FormatChecker
import json
from datetime import datetime

##############################################################################

def agsi_validate(inputfilepathname, schemafilepathname):

    # Basic AGSi validation library
    # No front end
    # No checks over and above what the JSON schema can check - to be added separately
    # Includes format check, as this is what AGSI requires
    #   - BUT this relies on relevant libraries being present (see jsonschema docs)
    #   - At time of writing, AGSi only requires date and uri checks
    #   - TODO: there is no warning if they are not (unless we can figure out a way of checking)
    # No line nr of error raised
    #  - Note: can be done but needs extra library (json_source_map?) - leave this to applications?
    #       -  This is difficult in a JSON file as it depends on the formatting
    #       -  Instead we provide the json_path which points to the error location
    
    # INPUT
    # File to be validated (file path as str)
    # AGSi JSON schema file (file path as str)
    #  - Note: TODO: consider directly calling original on Gitlab, e.g. if None provided
    
    # OUTPUT
    # Success flag
    # Headline result (string)
    # List of errors: message and JSON path (as tuple)
    #  - Note: possibly consider passing the full error info too?
    # Run metadata, as a simple dictionary
    
    
    # Version number to return in metadata
    # TODO: I'm guessing that there may be a better way of doing this
    version = '0.0.1'
    
    # Before anything else, collect the basic metadata to be returned
    # More will be added as we go along
    metadata = {}
    metadata['Title'] = 'AGSi validator output'
    metadata['Validator version'] = version
    metadata['Input file'] = inputfilepathname
    metadata['AGSi JSON schema file'] = schemafilepathname  # TODO: Need to amend if we later allow this to be grabbed from Gitlab instead
    datenow = datetime.now()
    metadata['Date time'] = datenow.strftime("%Y-%m-%d %H:%M:%S")
  
    
    # Default success flag set - we change it to True if the validation is completed
    # Note that success = True even if validation shows errors
    success = False
    # Summary result of validation, i.e. clean or nr of errors, or exception, will be given using this:
    headline = ''
    # We will collect errors in this array, although most will also be printed
    # Includes info on exceptions that stop execution as well as the validation errors
    errorlist = []

    # TODO: I think that we need to check for a well formed JSON file first.

    # Read JSON to dict for the AGSi schema
    # Then check that all is well with it (we will stop if not)
    try:
        with open(schemafilepathname,'r') as jsondata:
            agsischema = json.load(jsondata)
            # Wanted to use the following, but can't figure out how!
            # validators.validator_for(agsischema)
            # For now assume only using 2019-09 - TODO: improve later
        # Check on validity of schema as a JSON schema only
        # Note: Schema needs to be really bad to not pass this first test
        #       It seems that almost any valid JSON still passes!
        Draft201909Validator.check_schema(agsischema)
        # This sets up a shortcut to the validator object we will use later 
        # format_checker activates format check (default without is no format check)
        v = Draft201909Validator(agsischema, format_checker=FormatChecker())
        
    # If the above fails, trap exceptions and return useful info
    except FileNotFoundError as e:
        msg = 'WARNING! JSON schema file not found.'
        msg = msg + '\nProcess terminated before AGSi validation.'
        headline = msg
        print(headline)
        errorlist = [e.strerror + ': ' + e.filename]
    except SchemaError as e:
        # It seems as though schema file has to be really bad to be rejected here!
        msg = 'WARNING! JSON schema selected or provided is not valid.'
        msg = msg + '\nProcess terminated before AGSi validation.'
        headline = msg
        print(headline)
        errorlist = [e.message]
    except:
        msg = 'WARNING! Unexpected error raised.'
        msg = msg + '\nProcess terminated before AGSi validation.'
        headline = msg
        print(headline)
    # If any exceptions raised, we skip to the return at the end of function

    else: # If no exceptions raised, then carry on to main validation...
    
    # Read JSON to dict for AGSi file to be validated
    # Use try so we can intercept bad file or bad JSON
        try:
            with open(inputfilepathname,'r') as jsondata:
                agsidata = json.load(jsondata) 
        # Exceptions if something amiss with file - return with useful info                   
        except ValueError as e:
            msg = 'WARNING! File is not a valid JSON file.'
            msg = msg + '\nProcess terminated before AGSi validation.'
            msg = msg + '\nSee detailed results for errors raised.'
            headline = msg
            print(headline)
            errorlist = [e]
        except FileNotFoundError as e:
            msg = 'WARNING! File not found.'
            msg = msg + '\nProcess terminated before AGSi validation.'
            headline = msg
            print(headline)
            errorlist = [e.strerror + ': ' + e.filename]
        except:
            msg = 'WARNING! Unexpected error raised.'
            headline = msg
            print(headline)
        # If any exceptions raised, we skip to the return at the end of function
        else:
            # Only continue if all is well with AGSi file, i.e it is valid JSON (should be safe)
            success = True # success is True if validation works - even if invalid file!
            # First quick check if passes validation:           
            if v.is_valid(agsidata):           
                headline = 'OK. Input file is valid AGSi.'
                print(headline)

            else:
                # If not, say so, then get the errors
                errors = v.iter_errors(agsidata)  # get all validation errors
                # Now parse into a more user friendly list to return
                # For now, we simply returning a tuple of the human readable message and the json_path,
                # . The latter should come in useful when seeking to track down the error
                errorlist = []
                for e in errors:
                    errorlist.append((e.message,e.json_path))
                msg = 'WARNING. Input file is NOT valid AGSi.'
                msg = msg + '\n' + str(len(errorlist)) + ' errors identified.' 
                msg = msg + '\nSee detailed results for errors raised.'
                headline = msg
                print(msg)

    return success, headline, errorlist, metadata

if __name__ == "__main__":
    import os
    from tkinter import Tk, filedialog

    # Choose one of these
    #schemafilepathname = 'schemas/AGSi_JSONSchema_v1-0-0_2019-09.json'

    inputschemafilepathname = filedialog.askopenfilename(initialdir=os.getcwd()+'/schemas',
                                                   title='Select schema file',
                                                   filetypes=[('JSON files', '*.json')])


    # For testing try:
    # 'schemas/junk.json'
    # 'schemas/AGSi_JSONSchema_v1-0-0_2019-09_broken.json'
    # This version includes correct (JSON) $schema reference at the top - no other changes
    # Only relevant if we get the method for automatically determining the schema for validation up and running
    # 'schemas/AGSi_JSONSchema_v1-0-0_2019-09_corrected.json'

    # Explicitly open root window, then destroy at end so it doesn't hang around
    # root =  Tk()
    # Select a file
    inputfilepathname = filedialog.askopenfilename(initialdir=os.getcwd()+'/testdata',
                                                   title='Select AGSi file to validate',
                                                   filetypes=[('JSON files', '*.json')])
    # root.destroy()

    ############################################

    success, headline, errorlist, metadata = agsi_validate(inputfilepathname, inputschemafilepathname)
    print('success is: ' + str(success))
    print('headline is: ' + str(headline))
    print('errorlist is: ' + str(errorlist))
    print('metadata is: ' + str(metadata))

