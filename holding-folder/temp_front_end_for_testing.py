# -*- coding: utf-8 -*-
"""
Created on Mon May 22 17:48:24 2023

@author: DigitalGeotechnical
"""

# This is a bit of code that simply calls the main library of functions, to test them out

# Import main library

from agsi_validator import agsi_validate


######################################################################
# 

from tkinter import Tk, filedialog

# Choose one of these
schemafilepathname = 'static/AGSi_JSONSchema_v1-0-0_2019-09.json'

# For testing try:
#schemafilepathname = 'static/junk.json'
#schemafilepathname = 'static/AGSi_JSONSchema_v1-0-0_2019-09_broken.json'
# This version includes correct (JSON) $schema reference at the top - no other changes
    # Only relevant if we get the method for automatically determining the schema for validation up and running
#schemafilepathname = 'static/AGSi_JSONSchema_v1-0-0_2019-09_corrected.json'


#Explicitly open root window, then destroy at end so it doesn't hang around
root =  Tk()
# Select a file
inputfilepathname = filedialog.askopenfilename(initialdir = '',
                                  title = 'Select AGSi file to validate',
                                  filetypes = [('JSON files','*.json')])
root.destroy()

############################################


success, headline, errorlist, metadata = agsi_validate(inputfilepathname, schemafilepathname)